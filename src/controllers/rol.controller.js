import Rol from '../models/MRol'

export async function createRol(req, res) {
  const { name, tipo_rol } = req.body
  const estatus = 1
  try {
    let newRol = await Rol.create({
      name,
      tipo_rol,
      estatus
    }, {
      fields: ['name', 'tipo_rol','estatus']
    })
    if(newRol){
      res.json(newRol)
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      msg: 'Error',
      data: {}
    })
  }
}