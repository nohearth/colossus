import TipoEquipo from '../models/MtipoEquipo'

export async function createTipoEquipo(req, res) {
  const { name, description } = req.body
  try {
    let newTipoEquipo = await TipoEquipo.create({
      name,
      description
    }, {
      fields: ['name', 'description']
    })
    if(newTipoEquipo) {
      res.json({
        msg: 'Tipo de Equipo registrado',
        data: newTipoEquipo
      })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Error',
      data: {}
    })
  }
}
