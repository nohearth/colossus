import Modelo from '../models/Mmodelo'

export async function createModelo(req, res) {
  const { name, description, marcaid, tipoequipoid } = req.body
  try {
    let newModelo = await Modelo.create({
      name,
      description,
      marcaid,
      tipoequipoid
    }, {
      fields: ['name', 'description', 'marcaid', 'tipoequipoid']
    })
    if(newModelo) {
      res.json({
        msg: 'Modelo creado',
        data: newModelo
      })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Error',
      data: {}
    })
  }
}

export async function getModelbyMarca(req, res) {
  const { marcaid } = req.params
  try {
    const findAllModel = await Modelo.findAll({
      attributes: ["name", "description", "marcaid", "tipoequipoid"],
      where : {
        marcaid
      }
    })
    res.json(findAllModel)
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Error',
      data: {}
    })
  }  
}

export async function getModelbyTipo(req, res) {
  const { tipoequipoid } = req.params
  try {
    const findAllModel = await Modelo.findAll({
      attributes: ["name", "description", "marcaid", "tipoequipoid"],
      where : {
        tipoequipoid
      }
    })
    res.json(findAllModel)
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Error',
      data: {}
    })
  }  
}
