import Servicio from '../models/Mservicio'

export async function createServicio(req, res) {
  const { name, description } = req.body
  try {
    let newServicio = await Servicio.create({
      name,
      description
    }, {
      fields: ['name', 'description']
    })
    if(newServicio) {
      return res.json({
        msg: 'Servicio creado',
        data: newServicio
      })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Error',
      data: {}
    })
  }
}