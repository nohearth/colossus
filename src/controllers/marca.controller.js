import Marca from '../models/Mmarca'
import Modelo from '../models/Mmodelo';
import Sequelize from 'sequelize'
import TipoEquipo from '../models/MtipoEquipo';

export async function createMarca(req, res) {
  const { name, description } = req.body
  try {
    let newMarca = await Marca.create({
      name,
      description
    }, {
      fields: ['name', 'description']
    })
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Error',
      data: {}
    })
  }
}

export async function getMarcabyModel(req, res) {
  const { id } = req.params
  try {
    const findAllMarca = await Marca.findAll({
      attributes: ["id", "name"],
      where: {
        id
      }
    })
    res.json(findAllMarca)
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Error',
      data: {}
    })
  }
}

export async function getMarcabyModelbyTipo(req, res) {
  const { tipoequipoId } = req.params
  try {
    const findMarca = await Marca.findAll({
      attributes: ['id', 'name'],
      include : [{
        model: Modelo,
        attributes: [],
        where: {
          tipoequipoId
         }
      }]
    })
    res.json(findMarca)
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Error',
      data: {}
    })
  }
}
