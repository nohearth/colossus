import express, { json } from 'express'
import morgan from 'morgan'

//Rutas importadas
import servRoute from './routes/Rservicios'
import tipEqRoute from './routes/RtipoEquipo'
import modelRoute from './routes/Rmodelos'
import markRoute from './routes/Rmarcas'
import rolRoute from './routes/RRol'

const app = express()

//middlewares
app.use(morgan('dev'))
app.use(json())

//Rutas
app.use('/api/services', servRoute)
app.use('/api/tipoequipo', tipEqRoute)
app.use('/api/modelo', modelRoute)
app.use('/api/marca', markRoute)
app.use('/api/rol', rolRoute)

export default app