import Sequelize from 'sequelize'
import {sequelize} from '../db/database'

const ServicioEquipo = sequelize.define('servicioequipos',{
  idservico: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  idtipoequipo: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  description: {
    type: Sequelize.TEXT
  }
}, {
  timestamps: false
})

export default ServicioEquipo