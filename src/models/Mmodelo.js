import Sequelize from 'sequelize'
import {sequelize} from '../db/database'

import Marca from './Mmarca'
import TipoEquipo from './MtipoEquipo'

const Modelo = sequelize.define('modelos',{
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  name: {
    type: Sequelize.TEXT
  },
  description: {
    type: Sequelize.TEXT
  }
}, {
  timestamps: false
})

Marca.hasMany(Modelo)
Modelo.belongsTo(Marca)

TipoEquipo.hasMany(Modelo)
Modelo.belongsTo(TipoEquipo)

Modelo.sync()

export default Modelo