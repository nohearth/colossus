import Sequelize from 'sequelize'
import {sequelize} from '../db/database'

const Marca = sequelize.define('marcas',{
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  name: {
    type: Sequelize.TEXT
  },
  description: {
    type: Sequelize.TEXT
  }
}, {
  timestamps: false
})

Marca.sync()

export default Marca