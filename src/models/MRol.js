import Sequelize from 'sequelize'
import {sequelize} from '../db/database'

const Rol = sequelize.define('roles',{
  name: Sequelize.STRING(20),
  tipo_rol: Sequelize.STRING(30),
  estatus: Sequelize.INTEGER
}, {
  timestamps: false
})

Rol.sync()

export default Rol