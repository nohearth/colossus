import Sequelize from 'sequelize'
import {sequelize} from '../db/database'

const Servicio = sequelize.define('servicio',{
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  name: Sequelize.TEXT,
  description: Sequelize.TEXT,
}, {
  timestamps: false
})

//Servicio.hasMany(ServicioEquipo, {foreingKey: 'idservicio', sourceKey: 'id'})
//ServicioEquipo.belongsTo(Servicio, {foreingKey: 'idservicio', sourceKey: 'id'})

export default Servicio