import Sequelize from 'sequelize'
import {sequelize} from '../db/database'

const TipoEquipo = sequelize.define('tipoequipos',{
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  name: {
    type: Sequelize.TEXT
  },
  description: {
    type: Sequelize.TEXT
  }
}, {
  timestamps: false
})

TipoEquipo.sync()

export default TipoEquipo