import { Router } from 'express'

import { createServicio } from '../controllers/servicio.controller'

const router = Router()

router.post('/', createServicio)

export default router