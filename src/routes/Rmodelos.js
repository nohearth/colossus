import { Router } from 'express'

import {createModelo, getModelbyMarca, getModelbyTipo } from '../controllers/modelo.controller'

const router = Router()

router.post('/', createModelo)

// api/modelo/tipo/:tipoid
router.get('/tipo/:tipoequipoid', getModelbyTipo)

export default router