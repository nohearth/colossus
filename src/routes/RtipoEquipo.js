import { Router } from 'express'

import { createTipoEquipo } from '../controllers/tipoequipo.controller'

const router = Router()

router.post('/', createTipoEquipo)

export default router