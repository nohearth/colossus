import { Router } from 'express'

import { createMarca, getMarcabyModel, getMarcabyModelbyTipo } from '../controllers/marca.controller'
const router = Router()

router.post('/', createMarca)

router.get('/:id', getMarcabyModel)

router.get('/tipo/:tipoequipoId', getMarcabyModelbyTipo)

export default router